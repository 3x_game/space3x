const std = @import("std");

pub fn build(b: *std.Build) !void {
    const module_name = "space3x";
    // we want to create a wasm32 freestanding target by default
    const wasm_target = std.Target.Query{ .os_tag = .freestanding, .cpu_arch = .wasm32, .abi = .none };
    const target = b.standardTargetOptions(.{ .default_target = wasm_target });

    const optimize = b.standardOptimizeOption(.{});

    const wasm_module = b.addExecutable(.{ .name = module_name, .root_source_file = b.path("src/root.zig"), .target = target, .optimize = optimize });
    wasm_module.rdynamic = true;
    wasm_module.entry = .disabled;
    wasm_module.export_memory = true;
    b.installArtifact(wasm_module);

    // check step
    const wasm_module_check = b.addExecutable(.{ .name = module_name, .root_source_file = b.path("src/root.zig"), .target = target, .optimize = optimize });
    wasm_module_check.rdynamic = true;
    wasm_module_check.entry = .disabled;
    wasm_module_check.export_memory = true;
    const check = b.step("check", "Check if the wasm module compiles");
    check.dependOn(&wasm_module_check.step);

    // Creates a step for unit testing. This only builds the test executable
    // but does not run it.
    const test_target = b.resolveTargetQuery(std.Target.Query{ .os_tag = .linux, .abi = .gnu, .cpu_arch = .x86_64 });
    const test_optimize = std.builtin.OptimizeMode.Debug;
    const lib_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/root.zig"),
        .target = test_target,
        .optimize = test_optimize,
    });
    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);

    // copy artifact into static folder
    const path_to_module = "./zig-out/bin/" ++ module_name ++ ".wasm";
    const cp_cmd = b.addSystemCommand(&[_][]const u8{ "cp", path_to_module, "./static/" });
    cp_cmd.step.dependOn(b.getInstallStep());
    var copy_step = b.step("copy", "Copy the wasm module to the static folder");
    copy_step.dependOn(&cp_cmd.step);

    // watch step
    const watch_cmd = b.addSystemCommand(&[_][]const u8{"./watch.sh"});
    var watch_step = b.step("watch", "Watches changes and rebuilds and reloads browser");
    watch_step.dependOn(&watch_cmd.step);
}
