// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

const basic_primitives = @import("basic_primitives.zig");
const Point = basic_primitives.Point;
const Color = basic_primitives.Color;
const Rectangle = basic_primitives.Rectangle;
const Circle = basic_primitives.Circle;

frame_buffer: []u32,
width: u16,
height: u16,

const SwRasterizer = @This();

// TODO: Add check that buffer size does fit otherwise return an error.
pub fn new(buffer: []u32, width: usize, height: usize) SwRasterizer {
    return SwRasterizer{ .frame_buffer = buffer, .width = width, .height = height };
}

pub fn pixel(self: *const SwRasterizer, point: Point) ?Color {
    if (point.x > 0 and point.x < self.width) {
        if (point.y > 0 and point.y < self.height) {
            return Color.from_u32(self.frame_buffer[point.x + point.y * self.width]);
        }
    }
    return null;
}

fn put_pixel(self: *SwRasterizer, point: Point, color: Color) bool {
    if (point.x < self.width) {
        if (point.y < self.height) {
            const x: usize = point.x;
            const y: usize = point.y;
            self.frame_buffer[x + y * self.width] = color.to_u32();
            return true;
        }
    }
    return false;
}

fn put_horizontal_pixels(self: *SwRasterizer, point: Point, length: u16, color: Color) void {
    var x = point.x;
    const y = point.y;
    const max_x = x + length;

    while (x < max_x) {
        _ = self.put_pixel(Point{ .x = x, .y = y }, color);
        x += 1;
    }
}

pub fn fill(self: *SwRasterizer, color: Color) void {
    const color_value = color.to_u32();

    for (self.frame_buffer) |*pixel_value| {
        pixel_value.* = color_value;
    }
}

pub fn draw_rectangle(self: *SwRasterizer, rectangle: *const Rectangle) void {
    var y = rectangle.upper_left_corner.y;
    const max_y = rectangle.upper_left_corner.y + rectangle.height;
    // don't draw if alpha is zero. However, don't blend either.
    if (rectangle.color.alpha != 0) {
        while (y < max_y) {
            self.put_horizontal_pixels(Point.new(rectangle.upper_left_corner.x, y), rectangle.width, rectangle.color);
            y += 1;
        }
    }
}

pub fn draw_circle(self: *SwRasterizer, circle: *const Circle) void {
    // midpoint circle algorithm
    // concept / idea from https://www.youtube.com/watch?app=desktop&v=hpiILbMkF9w
    var x: i16 = 0;
    var y: i16 = @intCast(circle.radius);
    y = -y;
    var decision_parameter = y;
    while (x < -y) {
        if (decision_parameter >= 0) {
            y += 1;
            const decision_parameter_step = ((x + y) << 1) + 1;
            decision_parameter += decision_parameter_step;
        } else {
            const decision_parameter_step = (x << 1) + 1;
            decision_parameter += decision_parameter_step;
        }

        const cx: i16 = @intCast(circle.center.x);
        const cy: i16 = @intCast(circle.center.y);
        const color = circle.color;

        var min_x = cx - x;
        var length: u16 = @intCast(x << 1); //=cx+x-(cx-x)=cx-cx+x-(-x)=2x
        if (min_x < 0) {
            length -= @intCast(-min_x);
            min_x = 0;
        }
        var point = Point.new(@intCast(min_x), @intCast(cy + y));
        self.put_horizontal_pixels(point, length, color);

        point = Point.new(@intCast(min_x), @intCast(cy - y));
        self.put_horizontal_pixels(point, length, color);

        // y is negative (it starts at -r and is incremented from there on).
        min_x = cx + y;
        length = @intCast((-y) << 1); //=cx-y-(cx+y)=cx-cx+y-(-y)=-2y
        if (min_x < 0) {
            const subs: u16 = @intCast(-min_x);
            if (length >= subs) {
                length -= subs;
            }
            min_x = 0;
        }
        point = Point.new(@intCast(min_x), @intCast(cy + x));
        self.put_horizontal_pixels(point, length, color);

        point = Point.new(@intCast(min_x), @intCast(cy - x));
        self.put_horizontal_pixels(point, length, color);

        x += 1;
    }
}
