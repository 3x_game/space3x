// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

const basic_primitives = @import("basic_primitives.zig");
const Rectangle = basic_primitives.Rectangle;
const Point = basic_primitives.Point;
const Color = basic_primitives.Color;

fn Character(number_of_rectangles: u4) type {
    return struct {
        rectangles: [number_of_rectangles]Rectangle,
        const Self = @This();
    };
}

fn scale(value: u16, font_size: u16) u16 {
    const normed_size = 100;
    return value * font_size / normed_size;
}

pub fn zero(upper_left_corner: Point, color: Color, font_size: u16) Character(4) {
    const CharType = Character(4);
    // the upper rectangle is defined below
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    var point = Point.new(point_x, point_y);
    const long_side_length = scale(60, font_size);
    var width = long_side_length;
    const short_side_length = scale(10, font_size);
    var height = short_side_length;
    const upper_rect = Rectangle.new(point, width, height, color);
    // the left rectangle is defined below
    point.y = point.y + short_side_length;
    width = short_side_length;
    height = long_side_length;
    const left_rect = Rectangle.new(point, width, height, color);
    // the lower rectangle is defined below
    point.y = point.y + long_side_length;
    width = long_side_length;
    height = short_side_length;
    const lower_rect = Rectangle.new(point, width, height, color);
    // the right rectangle is defined below
    point.x = point_x + long_side_length - short_side_length;
    point.y = point_y + short_side_length;
    width = short_side_length;
    height = long_side_length;
    const right_rect = Rectangle.new(point, width, height, color);

    return CharType{ .rectangles = [_]Rectangle{ upper_rect, left_rect, right_rect, lower_rect } };
}

pub fn one(upper_left_corner: Point, color: Color, font_size: u16) Character(2) {
    const CharType = Character(2);
    // create the small rect
    const point_x = upper_left_corner.x + scale(40, font_size);
    const point_y = upper_left_corner.y + scale(20, font_size);
    var point = Point.new(point_x, point_y);
    var height = scale(10, font_size);
    const width = scale(10, font_size);
    const small_rect = Rectangle.new(point, width, height, color);
    // create the large rect
    point.x = point.x + width;
    point.y = point.y - height;
    height = scale(80, font_size);
    const large_rect = Rectangle.new(point, width, height, color);

    return CharType{ .rectangles = [_]Rectangle{ small_rect, large_rect } };
}

pub fn two(upper_left_corner: Point, color: Color, font_size: u16) Character(5) {
    const CharType = Character(5);
    // create the side lengths needed by all rectangles
    const longest_side = scale(60, font_size);
    const shortest_side = scale(10, font_size);
    const mid_side = scale(30, font_size);
    const small_side = scale(20, font_size);
    // create coordinates of the upper rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // create point as variable, since it is re-used throughout all rectangles
    var point = Point.new(point_x, point_y);
    // upper rectangle
    const upper_rectangle = Rectangle.new(point, longest_side, shortest_side, color);
    // mid rectangle
    point.y = point.y + mid_side + shortest_side;
    const mid_rectangle = Rectangle.new(point, longest_side, shortest_side, color);
    // bottom rectangle
    point.y = point.y + small_side + shortest_side;
    const bottom_rectangle = Rectangle.new(point, longest_side, shortest_side, color);
    // right rectangle
    point.x = point.x + longest_side - shortest_side;
    point.y = point_y + shortest_side;
    const right_rectangle = Rectangle.new(point, shortest_side, mid_side, color);
    // left rectangle
    point.x = point_x;
    point.y = point.y + mid_side + shortest_side;
    const left_rectangle = Rectangle.new(point, shortest_side, small_side, color);

    return CharType{ .rectangles = [5]Rectangle{ upper_rectangle, right_rectangle, mid_rectangle, left_rectangle, bottom_rectangle } };
}

pub fn three(upper_left_corner: Point, color: Color, font_size: u16) Character(4) {
    // define all side lengths use by all rectangles
    const longest_side = scale(80, font_size);
    const mid_side = scale(50, font_size);
    const shortest_side = scale(10, font_size);
    // define the point of the upper rectangle first
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    var point = Point.new(point_x, point_y);
    // upper rectangle
    const upper_rectangle = Rectangle.new(point, mid_side, shortest_side, color);
    // mid rectangle
    point.y = point.y + shortest_side + scale(20, font_size);
    const mid_rectangle = Rectangle.new(point, mid_side, shortest_side, color);
    // lower rectangle
    point.y = point.y + shortest_side + scale(30, font_size);
    const lower_rectangle = Rectangle.new(point, mid_side, shortest_side, color);
    // right rectangle
    point.y = point_y;
    point.x = point.x + mid_side;
    const right_rectangle = Rectangle.new(point, shortest_side, longest_side, color);

    const CharType = Character(4);
    return CharType{ .rectangles = [_]Rectangle{ upper_rectangle, right_rectangle, mid_rectangle, lower_rectangle } };
}

pub fn four(upper_left_corner: Point, color: Color, font_size: u16) Character(3) {
    // define all sides used / re-used by all rectangles
    const shortest_side = scale(10, font_size);
    const longest_side = scale(80, font_size);
    const mid_side = scale(50, font_size);
    const small_side = scale(30, font_size);
    // create the coordinates for the left rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define mutable variable point to be re-used for all rectangles
    var point = Point.new(point_x, point_y);
    // define left rectangle
    const left_rectangle = Rectangle.new(point, shortest_side, small_side, color);
    // define middle rectangle
    point.y = point.y + small_side;
    const mid_rectangle = Rectangle.new(point, mid_side, shortest_side, color);
    // define right rectangle
    point.x = point.x + mid_side;
    point.y = point_y;
    const right_rectangle = Rectangle.new(point, shortest_side, longest_side, color);
    // create the character type and return the number
    const CharType = Character(3);
    return CharType{ .rectangles = [_]Rectangle{ left_rectangle, mid_rectangle, right_rectangle } };
}

pub fn five(upper_left_corner: Point, color: Color, font_size: u16) Character(5) {
    // define all sides used in all rectangles
    const longest_side = scale(60, font_size);
    const shortest_side = scale(10, font_size);
    const mid_side = scale(30, font_size);
    const small_side = scale(20, font_size);
    // define the coordinates of the most upper and most left rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define the mutable point variable, re-used for all rectangles
    var point = Point.new(point_x, point_y);
    // define the upper rectangle
    const upper_rect = Rectangle.new(point, longest_side, shortest_side, color);
    // define the mid rectangle
    point.y = point.y + shortest_side + small_side;
    const mid_rect = Rectangle.new(point, longest_side, shortest_side, color);
    // define bottom rectangle
    point.y = point.y + shortest_side + mid_side;
    const bottom_rect = Rectangle.new(point, longest_side, shortest_side, color);
    // define left rectangle
    point.y = point_y + shortest_side;
    const left_rect = Rectangle.new(point, shortest_side, small_side, color);
    // define right rectangle
    point.y = point_y + 2 * shortest_side + small_side;
    point.x = point.x + longest_side - shortest_side;
    const right_rect = Rectangle.new(point, shortest_side, mid_side, color);

    const CharType = Character(5);
    return CharType{ .rectangles = [_]Rectangle{ upper_rect, left_rect, mid_rect, right_rect, bottom_rect } };
}

pub fn six(upper_left_corner: Point, color: Color, font_size: u16) Character(5) {
    // define all side lengths used by the rectangles
    const longest_side = scale(60, font_size);
    const shortest_side = scale(10, font_size);
    const mid_side = scale(40, font_size);
    const small_side = scale(30, font_size);
    // define point of the most left and most upper rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define mutable point that is used to create all rectangles
    var point = Point.new(point_x, point_y);
    // upper rectangle
    const upper_rect = Rectangle.new(point, longest_side, shortest_side, color);
    // left rectangle
    point.y = point.y + shortest_side;
    const left_rect = Rectangle.new(point, shortest_side, longest_side, color);
    // bottom rectangle
    point.y = point.y + longest_side;
    const bottom_rect = Rectangle.new(point, longest_side, shortest_side, color);
    // right rectangle
    point.y = point_y + shortest_side + small_side;
    point.x = point_x + shortest_side + mid_side;
    const right_rect = Rectangle.new(point, shortest_side, small_side, color);
    // mid rectangle
    point.x = point_x + shortest_side;
    const mid_rect = Rectangle.new(point, mid_side, shortest_side, color);

    const CharType = Character(5);
    return CharType{ .rectangles = [_]Rectangle{ upper_rect, left_rect, bottom_rect, right_rect, mid_rect } };
}

pub fn seven(upper_left_corner: Point, color: Color, font_size: u16) Character(2) {
    // define side lengths for all rectangles
    const longest_side = scale(70, font_size);
    const shortest_side = scale(10, font_size);
    const mid_side = scale(60, font_size);
    // define point for most upper and most left rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define mutable point variable re-used by all rectangles
    var point = Point.new(point_x, point_y);
    // upper rectangle
    const upper_rect = Rectangle.new(point, mid_side, shortest_side, color);
    // right rectangle
    point.x = point.x + mid_side - shortest_side;
    point.y = point.y + shortest_side;
    const right_rect = Rectangle.new(point, shortest_side, longest_side, color);

    const CharType = Character(2);
    return CharType{ .rectangles = [_]Rectangle{ upper_rect, right_rect } };
}

pub fn eight(upper_left_corner: Point, color: Color, font_size: u16) Character(5) {
    // sides used in all rectangles
    const longest_side = scale(80, font_size);
    const shortest_side = scale(10, font_size);
    const mid_side = scale(40, font_size);
    // define the point of the most left most upper rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define mutable point variable re-used by all rectangles
    var point = Point.new(point_x, point_y);
    // define left rectangle
    const left_rect = Rectangle.new(point, shortest_side, longest_side, color);
    // define upper rectangle
    point.x = point.x + shortest_side;
    const upper_rect = Rectangle.new(point, mid_side, shortest_side, color);
    // define right rectangle
    point.x = point.x + mid_side;
    const right_rect = Rectangle.new(point, shortest_side, longest_side, color);
    // define middle rectangle
    point.x = point_x + shortest_side;
    point.y = point.y + scale(30, font_size);
    const mid_rect = Rectangle.new(point, mid_side, shortest_side, color);
    // define bottom rect
    point.y = point_y + longest_side - shortest_side;
    const bottom_rect = Rectangle.new(point, mid_side, shortest_side, color);

    const CharType = Character(5);
    return CharType{ .rectangles = [_]Rectangle{ left_rect, upper_rect, right_rect, mid_rect, bottom_rect } };
}

pub fn nine(upper_left_corner: Point, color: Color, font_size: u16) Character(5) {
    // define all sides used by the rectangles
    const longest_side = scale(70, font_size);
    const shortest_side = scale(10, font_size);
    const small_side = scale(20, font_size);
    const big_side = scale(60, font_size);
    const mid_side = scale(50, font_size);
    // define point of the most left most upper rectangle
    const point_x = upper_left_corner.x + scale(20, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define mutable point variable, re-used by all rectangles
    var point = Point.new(point_x, point_y);
    // define upper rectangle
    const upper_rect = Rectangle.new(point, big_side, shortest_side, color);
    // define right rectangle
    point.x = point.x + mid_side;
    point.y = point.y + shortest_side;
    const right_rect = Rectangle.new(point, shortest_side, longest_side, color);
    // define left rectangle
    point.x = point_x;
    point.y = point_y + shortest_side;
    const left_rect = Rectangle.new(point, shortest_side, small_side, color);
    // mid rectangle
    point.y = point_y + small_side + shortest_side;
    const mid_rect = Rectangle.new(point, mid_side, shortest_side, color);
    // bottom rectangle
    point.y = point_y + longest_side;
    const bottom_rect = Rectangle.new(point, mid_side, shortest_side, color);

    const CharType = Character(5);
    return CharType{ .rectangles = [_]Rectangle{ upper_rect, right_rect, left_rect, mid_rect, bottom_rect } };
}

pub fn period(upper_left_corner: Point, color: Color, font_size: u16) Character(1) {
    const point = Point.new(upper_left_corner.x + scale(40, font_size), upper_left_corner.y + scale(80, font_size));
    const side_length = scale(10, font_size);
    const rect = Rectangle.new(point, side_length, side_length, color);
    const CharType = Character(1);
    return CharType{ .rectangles = [_]Rectangle{rect} };
}

pub fn exclamation_mark(upper_left_corner: Point, color: Color, font_size: u16) Character(2) {
    // define all side lengths of used rectangles
    const longest_side = scale(60, font_size);
    const shortest_side = scale(10, font_size);
    // define coordinates of most upper rectangle
    const point_x = upper_left_corner.x + scale(40, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    // define mutable point variable re-used by all rectangles
    var point = Point.new(point_x, point_y);
    // define upper rectangle
    const upper_rect = Rectangle.new(point, shortest_side, longest_side, color);
    // define lower rectangle
    point.y = point.y + longest_side + shortest_side;
    const lower_rect = Rectangle.new(point, shortest_side, shortest_side, color);

    const CharType = Character(2);
    return CharType{ .rectangles = [_]Rectangle{ upper_rect, lower_rect } };
}

pub fn minus(upper_left_corner: Point, color: Color, font_size: u16) Character(1) {
    const point = Point.new(upper_left_corner.x + scale(20, font_size), upper_left_corner.y + scale(40, font_size));
    const rect = Rectangle.new(point, scale(60, font_size), scale(20, font_size), color);
    const CharType = Character(1);
    return CharType{ .rectangles = [_]Rectangle{rect} };
}

pub fn plus(upper_left_corner: Point, color: Color, font_size: u16) Character(3) {
    // define all side lengths used by all rectangles
    const longest_side = scale(60, font_size);
    const shortest_side = scale(20, font_size);
    // define the coordinates of the top and most left rectangle
    const point_x = upper_left_corner.x + scale(40, font_size);
    const point_y = upper_left_corner.y + scale(20, font_size);
    // define a mutable point re-used by all rectangles
    var point = Point.new(point_x, point_y);
    // define the upper rectangle
    const upper_rect = Rectangle.new(point, shortest_side, shortest_side, color);
    // define the middle rectangle
    point.x = point.x - shortest_side;
    point.y = point.y + shortest_side;
    const mid_rect = Rectangle.new(point, longest_side, shortest_side, color);
    // define lower rectangle
    point.x = point_x;
    point.y = point.y + shortest_side;
    const low_rect = Rectangle.new(point, shortest_side, shortest_side, color);

    const CharType = Character(3);
    return CharType{ .rectangles = [_]Rectangle{ upper_rect, mid_rect, low_rect } };
}

pub fn i(upper_left_corner: Point, color: Color, font_size: u16) Character(1) {
    const CharType = Character(1);
    const point_x = upper_left_corner.x + scale(40, font_size);
    const point_y = upper_left_corner.y + scale(10, font_size);
    const point = Point.new(point_x, point_y);
    const rect_width = scale(10, font_size);
    const rect_height = scale(80, font_size);
    const i_character = CharType{
        .rectangles = [_]Rectangle{Rectangle.new(point, rect_width, rect_height, color)},
    };
    return i_character;
}
