// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

const graphics = @import("root.zig");
const Grid = graphics.Grid;
const Point = graphics.Point;
const Color = graphics.Color;
const SwRasterizer = graphics.SwRasterizer;
const Rectangle = graphics.Rectangle;
const Circle = graphics.Circle;
const StrokedRectangle = graphics.StrokedRectangle;
const font = graphics.font;

const logging = @import("../logging.zig");
const log_info = logging.log_info;
const Number2Format = logging.NumberToFormat;

const Transparent = Color{ .red = 0, .blue = 0, .green = 0, .alpha = 0 };
const White = Color{ .red = 0xff, .blue = 0xff, .green = 0xff, .alpha = 0xff };

const Grid10x10 = Grid(10, 10);
const cell_size: u16 = 100;
const cell_stroke_width: u16 = 2;
grid: Grid10x10,

const Self = @This();

pub fn new() Self {
    return Self{ .grid = Grid10x10.new(Point.new(0, 0), cell_size, cell_size, Transparent, cell_stroke_width, White) };
}

pub fn get_grid(self: *Self) *Grid10x10 {
    return &self.grid;
}

pub fn draw(self: *Self, sw_rasterizer: *SwRasterizer) void {
    log_info("draw test page", .{});
    graphics.draw_stroked_rectangles(sw_rasterizer, self.grid.rectangles[0..]);
    self.draw_rect(sw_rasterizer);
    self.draw_stroked_rect(sw_rasterizer);
    self.draw_circle(sw_rasterizer);
    self.draw_punctuations(sw_rasterizer);
    self.draw_numbers(sw_rasterizer);
}

fn draw_numbers(self: *Self, sw_rasterizer: *SwRasterizer) void {
    var i: u16 = 0;
    const font_size = 100;
    while (i < 10) {
        const cell = self.grid.get_stroked_rectangle_at(i, 1);
        if (cell) |c| {
            const point = c.get_upper_left_corner();
            const rects = switch (i) {
                0 => zero: {
                    const zero = font.zero(point, Color{ .red = 0xff, .alpha = 0xff, .green = 0xff, .blue = 0 }, font_size);
                    break :zero &zero.rectangles;
                },
                1 => one: {
                    const one = font.one(point, Color{ .alpha = 0xff, .red = 0, .green = 0xff, .blue = 0xff }, font_size);
                    break :one &one.rectangles;
                },
                2 => two: {
                    const two = font.two(point, Color{ .alpha = 0xff, .red = 0x88, .green = 0x88, .blue = 0x88 }, font_size);
                    break :two &two.rectangles;
                },
                3 => three: {
                    const three = font.three(point, Color.rgb(0xc0, 0x0c, 0xc0), font_size);
                    break :three &three.rectangles;
                },
                4 => four: {
                    const four = font.four(point, Color.rgb(0x88, 0x44, 0xff), font_size);
                    break :four &four.rectangles;
                },
                5 => five: {
                    const five = font.five(point, Color.rgb(0x12, 0x34, 0x56), font_size);
                    break :five &five.rectangles;
                },
                6 => six: {
                    const six = font.six(point, Color.rgb(0x65, 0x43, 0x21), font_size);
                    break :six &six.rectangles;
                },
                7 => seven: {
                    const seven = font.seven(point, Color.rgb(0xfe, 0xdc, 0xba), font_size);
                    break :seven &seven.rectangles;
                },
                8 => eight: {
                    const eight = font.eight(point, Color.rgb(0xab, 0xcd, 0xef), font_size);
                    break :eight &eight.rectangles;
                },
                9 => nine: {
                    const nine = font.nine(point, Color.rgb(0xba, 0xdc, 0xfe), font_size);
                    break :nine &nine.rectangles;
                },
                else => @panic("More than 10 numbers on test page."),
            };
            graphics.draw_rectangles(sw_rasterizer, rects);
        }
        i += 1;
    }
}

fn draw_punctuations(self: *Self, sw_rasterizer: *SwRasterizer) void {
    log_info("draw_punctuation", .{});
    const font_size = 100;

    const punctuations = ".?!+-";
    for (punctuations, 0..) |punctuation, i| {
        const j: u16 = @truncate(i + 3);
        log_info("Punct. i=", Number2Format.new(j));
        const cell = self.grid.get_stroked_rectangle_at(j, 0);
        if (cell) |c| {
            const point = c.get_upper_left_corner();
            const rectangles = switch (punctuation) {
                '.' => period: {
                    const period = font.period(point, Color.rgb(0x77, 0x88, 0x99), font_size);
                    break :period &period.rectangles;
                },
                '!' => exclamation_mark: {
                    const exclamation_mark = font.exclamation_mark(point, Color.rgb(0x99, 0x77, 0x88), font_size);
                    break :exclamation_mark &exclamation_mark.rectangles;
                },
                '+' => plus: {
                    const plus = font.plus(point, Color.rgb(0x11, 0xff, 0x88), font_size);
                    break :plus &plus.rectangles;
                },
                '-' => minus: {
                    const minus = font.minus(point, Color.rgb(0x55, 0x66, 0x77), font_size);
                    break :minus &minus.rectangles;
                },
                else => not_implemented: {
                    const rects = [_]Rectangle{};
                    break :not_implemented &rects;
                },
            };
            graphics.draw_rectangles(sw_rasterizer, rectangles);
        }
    }
}

fn draw_rect(self: *Self, sw_rasterizer: *SwRasterizer) void {
    const cell = self.grid.get_stroked_rectangle_at(0, 0);
    if (cell) |c| {
        const point = c.get_upper_left_corner();
        const stroke_width = c.get_stroke_width();
        const rect_point = Point.new(point.x + stroke_width + cell_size / 10, point.y + stroke_width + cell_size / 10);
        const rect = Rectangle.new(rect_point, cell_size - 2 * cell_size / 10 - 2 * stroke_width, cell_size - 2 * cell_size / 10 - 2 * stroke_width, Color.from_u32(0xff0000ff));
        sw_rasterizer.draw_rectangle(&rect);
    }
}

fn draw_stroked_rect(self: *Self, sw_rasterizer: *SwRasterizer) void {
    const cell = self.grid.get_stroked_rectangle_at(1, 0);
    if (cell) |c| {
        const point = c.get_upper_left_corner();
        const stroke_width = c.get_stroke_width();
        const rect_point = Point.new(point.x + stroke_width + cell_size / 10, point.y + stroke_width + cell_size / 10);
        log_info("srect p.x=", Number2Format.new(rect_point.x));
        log_info("srect p.y=", Number2Format.new(rect_point.y));
        const width = cell_size - 2 * cell_size / 10 - 2 * stroke_width;
        log_info("srect width=", Number2Format.new(width));
        const height = cell_size - 2 * cell_size / 10 - 2 * stroke_width;
        log_info("srect height=", Number2Format.new(height));
        const stroked_rect = StrokedRectangle.new(rect_point, width, height, Color.from_u32(0xff00ff00), 5, Color.from_u32(0xffff0000));
        graphics.draw_rectangles(sw_rasterizer, &stroked_rect.rectangles);
    }
}

fn draw_circle(self: *Self, sw_rasterizer: *SwRasterizer) void {
    const cell = self.grid.get_stroked_rectangle_at(2, 0);
    if (cell) |c| {
        const point = Point.new(c.get_upper_left_corner().x + 50, c.get_upper_left_corner().y + 50);
        const circle = Circle.new(point, 40, Color.from_u32(0xffff00ff));
        sw_rasterizer.draw_circle(&circle);
    }
}
