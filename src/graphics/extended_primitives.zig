// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

const basic_primitves = @import("basic_primitives.zig");
const Color = basic_primitves.Color;
const Rectangle = basic_primitves.Rectangle;
const Point = basic_primitves.Point;

pub const StrokedRectangle = struct {
    rectangles: [5]Rectangle,

    pub fn get_upper_left_corner(self: *const StrokedRectangle) Point {
        return self.rectangles[0].upper_left_corner;
    }

    pub fn get_stroke_width(self: *const StrokedRectangle) u16 {
        return self.rectangles[0].height;
    }

    pub fn new(upper_left_corner: Point, width: u16, height: u16, inner_color: Color, stroke_width: u16, stroke_color: Color) StrokedRectangle {
        // This function creates a new stroked rectangle, i.e. a rectangle with a stroke.
        // Internally this is handled as an array of 5 rectangles, 4 for the stroke and one for the "actual" rectangle.
        // Below is an ASCII art diagram that describes how these rectangles are arranged:
        // __________________________________________________
        // |                                 /\             |
        // |   upper stroke rectangle        | stroke width |
        // |                                \/              |
        // _________________________________________________|
        // |           |                        |           |
        // | left      |                        | right     |
        // | stroke    |                        | stroke    |
        // | rectangle |    inner               | rectangle |
        // |           |    rectangle           |           |
        // |<-stroke-->|                        |           |
        // |  width    |                        |           |
        // |           |                        |           |
        // |___________|________________________|___________|
        // |                                                |
        // |   lower stroke rectangle                       |
        // |                                                |
        // _________________________________________________|

        // cap the stroke width so that it is not larger than the half or the actual height or width
        const capped_stroke_width = @min(@min(stroke_width, width / 2), height / 2);

        // the upper stroke is just a rectangle, which uses the upper_left_corner as its upper_left_corner
        const upper_stroke_rectangle = Rectangle.new(upper_left_corner, width, capped_stroke_width, stroke_color);

        // create a temporary upper left corner for the stroke on the left side
        var tmp_upper_left_corner = upper_left_corner;
        tmp_upper_left_corner.y += capped_stroke_width;
        // the stroke on the left side has the height of the original height minus twice the stroke_width
        const new_height = height - 2 * capped_stroke_width;
        const left_stroke_rectangle = Rectangle.new(tmp_upper_left_corner, capped_stroke_width, new_height, stroke_color);

        // re-use the temporary upper left corner variable for the lower stroke rectangle
        tmp_upper_left_corner.y += new_height;
        const lower_stroke_rectangle = Rectangle.new(tmp_upper_left_corner, width, capped_stroke_width, stroke_color);

        // re-use the temporary upper left corner variable for the inner rectangle.
        // this variable is "reset" to the upper left corner before the stroke width is added the x and y values of this point
        tmp_upper_left_corner = upper_left_corner;
        tmp_upper_left_corner.x += capped_stroke_width;
        tmp_upper_left_corner.y += capped_stroke_width;
        // the new width of the inner rectangle is the original width minus twice the stroke width
        const new_width = width - 2 * capped_stroke_width;
        const inner_rectangle = Rectangle.new(tmp_upper_left_corner, new_width, new_height, inner_color);

        // re-use the temporary upper left corner variable for the right stroke rectangle
        tmp_upper_left_corner.x += new_width;
        const right_stroke_rectangle = Rectangle.new(tmp_upper_left_corner, capped_stroke_width, new_height, stroke_color);
        const rectangles = [5]Rectangle{ upper_stroke_rectangle, left_stroke_rectangle, lower_stroke_rectangle, right_stroke_rectangle, inner_rectangle };
        return StrokedRectangle{ .rectangles = rectangles };
    }

    pub fn change_stroke_color(self: *StrokedRectangle, color: Color) void {
        for (self.rectangles[0..4]) |*rect| {
            rect.color = color;
        }
    }
};

pub fn Grid(comptime rows: u16, comptime cols: u16) type {
    return struct {
        const ROWS = rows;
        const COLS = cols;
        const Self = @This();
        const number_of_rectangles = ROWS * COLS;
        rectangles: [number_of_rectangles]StrokedRectangle,
        pub fn new(upper_left_corner: Point, cell_width: u16, cell_height: u16, cell_color: Color, stroke_width: u16, stroke_color: Color) Self {
            var rectangles: [number_of_rectangles]StrokedRectangle = undefined;
            var r: u16 = 0;
            while (r < ROWS) {
                var c: u16 = 0;
                while (c < COLS) {
                    const x_coordinate = upper_left_corner.x + c * cell_width;
                    const y_coordinate = upper_left_corner.y + r * cell_height;
                    const point = Point.new(x_coordinate, y_coordinate);
                    rectangles[c + r * COLS] = StrokedRectangle.new(point, cell_width, cell_height, cell_color, stroke_width, stroke_color);
                    c += 1;
                }
                r += 1;
            }
            return Self{ .rectangles = rectangles };
        }

        pub fn get_stroked_rectangle_at(self: *Self, row: u16, col: u16) ?*StrokedRectangle {
            if (row < ROWS and col < COLS) {
                const rectangleIndex = row + col * ROWS;
                return &self.rectangles[rectangleIndex];
            }
            return null;
        }
    };
}
