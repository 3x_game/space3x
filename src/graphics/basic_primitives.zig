// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

pub const Color = struct {
    red: u8 = 0,
    green: u8 = 0,
    blue: u8 = 0,
    alpha: u8 = 0,

    pub fn from_u32(value: u32) Color {
        const r: u8 = @intCast(0xFF & value);
        var tmp = value >> 8;
        const g: u8 = @intCast(0xFF & tmp);
        tmp = tmp >> 8;
        const b: u8 = @intCast(0xFF & tmp);
        tmp = tmp >> 8;
        const a: u8 = @intCast(0xFF & tmp);
        return Color{ .red = r, .green = g, .blue = b, .alpha = a };
    }

    pub fn to_u32(color: Color) u32 {
        const alpha: u32 = @intCast(color.alpha);
        var value: u32 = alpha << 8;
        value = value | color.blue;
        value = value << 8;
        value = value | color.green;
        value = value << 8;
        value = value | color.red;
        return value;
    }

    pub fn new(red: u8, green: u8, blue: u8, alpha: u8) Color {
        return Color{ .red = red, .green = green, .blue = blue, .alpha = alpha };
    }

    pub fn transparent() Color {
        return Color.new(0, 0, 0, 0);
    }

    pub fn rgb(red: u8, green: u8, blue: u8) Color {
        return Color.new(red, green, blue, 0xff);
    }
};

pub const Point = struct {
    x: u16 = 0,
    y: u16 = 0,

    pub fn new(x_value: u16, y_value: u16) Point {
        return Point{ .x = x_value, .y = y_value };
    }
};

pub const Rectangle = struct {
    upper_left_corner: Point,
    width: u16,
    height: u16,
    color: Color,

    pub fn new(upper_left_corner: Point, width: u16, height: u16, color: Color) Rectangle {
        return Rectangle{ .upper_left_corner = upper_left_corner, .width = width, .height = height, .color = color };
    }
};

pub const Circle = struct {
    center: Point,
    radius: u16,
    color: Color,

    pub fn new(center: Point, radius: u16, color: Color) Circle {
        return Circle{ .center = center, .radius = radius, .color = color };
    }
};
