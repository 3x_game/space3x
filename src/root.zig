// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

const CurrentGameStatus = @import("game_state/VolatileGameState.zig").CurrentGameStatus;
const graphics = @import("./graphics/root.zig");
const Point = graphics.Point;
const Color = graphics.Color;
const Rectangle = graphics.Rectangle;
const StrokedRectangle = graphics.StrokedRectangle;
const font = graphics.font;
const i = font.i;
const Grid = graphics.Grid;
const Circle = graphics.Circle;
const SwRasterizer = graphics.SwRasterizer;
const draw_rectangles = graphics.draw_rectangles;
const draw_stroked_rectangles = graphics.draw_stroked_rectangles;
// debugging / testing relevant
const logging = @import("logging.zig");
const log_error = logging.log_error;
const log_info = logging.log_info;
const NumberValue = logging.NumberValue;
const num2Format = logging.NumberToFormat.new;
const TestPage = @import("graphics/TestPage.zig");

// frame buffer
export const width: u16 = 1000;
export const height: u16 = 1000;
const frame_buffer_length: u32 = @as(u32, width) * @as(u32, height);
export var buffer: [frame_buffer_length]u32 = [_]u32{0} ** frame_buffer_length;

// game state
const persistent_game_state_buffer_length = 3;
export var persistent_game_state_buffer: [persistent_game_state_buffer_length]u8 = [_]u8{0} ** persistent_game_state_buffer_length;
var point: ?Point = null;

// logging
export const max_number_of_log_messages: u16 = 32;
export const max_length_log_message: u8 = logging.max_message_length;
export var number_of_log_messages: u16 = 0;
const message_buffer_length: usize = init: {
    const num_messages: usize = @intCast(max_number_of_log_messages);
    const msg_len: usize = @intCast(max_length_log_message + 1);
    const buf_len = num_messages * msg_len;
    break :init buf_len;
};
export var message_buffer: [message_buffer_length]u8 = [_]u8{0} ** message_buffer_length;

// No atomics here, since I don't expect multi-threading here.
var frame: u8 = 0;
var color_index: u3 = 0;
const colors = [5]Colors{ .black, .white, .red, .blue, .green };
var pixel_value: u32 = @intFromEnum(Colors.black);
var sw_rasterizer = SwRasterizer.new(&buffer, width, height);

var current_game_status = CurrentGameStatus.NotStarted;

const Colors = enum(u32) {
    white = 0xFF_FF_FF_FF,
    black = 0xFF_00_00_00,
    red = 0xFF_00_00_FF,
    green = 0xFF_00_FF_00,
    blue = 0xFF_FF_00_00,
    turquoise = 0xFF_c8_d5_30,
};

export fn init(game_state: u8) callconv(.C) void {
    logging.init(&message_buffer, &number_of_log_messages);
    log_info("Logging initialized", .{});
    current_game_status = @enumFromInt(game_state);
    sw_rasterizer.fill(Color.from_u32(@intFromEnum(Colors.black)));
    log_info("game initialized.", .{});
}

export fn update(mouse_down_x: u16, mouse_down_y: u16) callconv(.C) void {
    point = Point.new(mouse_down_x, mouse_down_y);
    log_info("Mouse x=", .{ .value = NumberValue{ .U16 = mouse_down_x } });
    log_info("Mouse y=", .{ .value = NumberValue{ .U16 = mouse_down_y } });
    log_info("game updated.", .{});
}

var toggle: bool = false;

export fn draw() callconv(.C) void {
    var test_page = TestPage.new();
    const grid = test_page.get_grid();
    const cell_size: u16 = 100;
    if (point) |p| {
        const row = p.x / cell_size;
        const col = p.y / cell_size;
        const stroked_rectangle = grid.get_stroked_rectangle_at(row, col);
        if (stroked_rectangle) |rect| {
            rect.change_stroke_color(Color.from_u32(@intFromEnum(Colors.green)));
        }
    }
    (&test_page).draw(&sw_rasterizer);
}
