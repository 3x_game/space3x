// copyright by huntrss@posteo.me
//
//  This file is part of space3x.
//
//  space3x is free software: you can redistribute it and/or modify it under the terms of
//  the GNU Affero General Public License as published by the Free Software Foundation, either
//  version 3 of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
//  A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License along with this
//  program. If not, see <https://www.gnu.org/licenses/>.

const formatBufInt = @import("std").fmt.formatIntBuf;
message_buffer: []u8,
max_number_of_messages: usize,
number_of_messages: *u16,
const Logger = @This();
pub const max_message_length: u8 = 31;
const step: u8 = max_message_length + 1;
const INFO_FLAG: u8 = 0;
const ERROR_FLAG: u8 = 0x80;
var instance: ?Logger = undefined;

pub const NumberType = enum {
    U8,
    U16,
};

pub const NumberValue = union(NumberType) {
    U8: u8,
    U16: u16,
};

pub const NumberToFormat = struct {
    value: ?NumberValue = null,
    pub fn new(value: anytype) NumberToFormat {
        const T = @TypeOf(value);
        if (T == u16) {
            return NumberToFormat{ .value = NumberValue{ .U16 = value } };
        } else if (T == u8) {
            return NumberToFormat{ .value = NumberValue{ .U8 = value } };
        } else {
            @compileError("Number type not supported for formatting");
        }
    }
};

pub fn init(message_buffer: []u8, number_of_message: *u16) void {
    const max_number_of_messages: usize = message_buffer.len / 128;
    instance = Logger{
        .message_buffer = message_buffer,
        .max_number_of_messages = max_number_of_messages,
        .number_of_messages = number_of_message,
    };
}

pub fn log_info(format: []const u8, integerToFormat: NumberToFormat) void {
    log(INFO_FLAG, format, integerToFormat);
}

pub fn log_error(format: []const u8, integerToFormat: NumberToFormat) void {
    log(ERROR_FLAG, format, integerToFormat);
}

fn log(log_level: u8, format: []const u8, integerToFormat: NumberToFormat) void {
    if (instance) |*logger| {
        if (logger.number_of_messages.* < logger.max_number_of_messages) {
            if (integerToFormat.value) |intValue| {
                var index = logger.number_of_messages.* * step;
                var msg_len: u8 = @min(max_message_length, format.len);
                index += 1;
                for (format[0..msg_len]) |c| {
                    logger.message_buffer[index] = c;
                    index += 1;
                }
                if (msg_len < max_message_length) {
                    switch (intValue) {
                        NumberType.U16 => |u16Value| {
                            var buf: [5]u8 = [_]u8{0} ** 5;
                            var bytes_written = formatBufInt(&buf, u16Value, 10, .lower, .{});
                            bytes_written = @min(bytes_written, max_message_length - msg_len);
                            for (buf[0..bytes_written]) |b| {
                                logger.message_buffer[index] = b;
                                msg_len += 1;
                                index += 1;
                            }
                        },
                        NumberValue.U8 => |u8Value| {
                            logger.message_buffer[index] = u8Value;
                            msg_len += 1;
                            index += 1;
                        },
                    }
                }
                msg_len = @min(max_message_length, msg_len);
                const message_start_index = logger.number_of_messages.* * step;
                logger.message_buffer[message_start_index] = msg_len | log_level;
                logger.number_of_messages.* += 1;
            } else {
                const msg_len = @min(max_message_length, format.len);
                var index = logger.number_of_messages.* * step;
                logger.message_buffer[index] = msg_len | log_level;
                index += 1;
                for (format[0..msg_len]) |c| {
                    logger.message_buffer[index] = c;
                    index += 1;
                }
                logger.number_of_messages.* += 1;
            }
        }
    }
}
