#!/bin/bash

closeAll() {
  echo "close all ..."
  kill $(jobs -p)
  exit 0
}

trap closeAll SIGINT

# start static web server
python -m http.server -d static &
STATIC_SERVER_PID=$!

# start web socket server
websocat -E -t ws-l:127.0.0.1:1234 broadcast:mirror: &
WEB_SOCKET_SERVER_PID=$!

# open browser
xdg-open http://127.0.0.0:8000 &

# start watchexec
watchexec -e zig,html --timings "zig build copy -Doptimize=ReleaseSmall && echo 'reload' |  websocat ws://127.0.0.1:1234 -1 --linemode-strip-newlines" &
WATCHEXEC_PID=$1

wait $STATIC_WEB_SERVER_PID $WEBSOCKET_SERVER_PID $WATCHEXEC_PID
