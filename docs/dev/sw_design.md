# Software Design of space3x game

This document contains the (rough) SW design of the space3x game.
Parts of this document may be outdated.
Therefore I plan to add a date when a specific section was added.
The diagrams in this document follow the concepts of the [c4model](https://c4model.com/) albeit more simplified.

## Container diagram

![C4 container diagram spac3x game](./sw_design_space3x_C4_container_diagram.drawio.svg "C4 container diagram space3x game")

## Component diagram

![C4 component diagram space3x game](./sw_design_space3x_C4_component_diagram.drawio.svg "C4 component diagram space3x game")

* space3x wasm module
* Html page with JavaScript glue code
* Some browser storage
* browser with web assembly runtime
* *Could also be a container diagram, need to check later*
* Load & Store
* Game init
* Game update
* Game draw
* Hot reload for developing
* Start menu
* Main menu
* Input handling
* Graphics
* Next round
* Restart round
* Science
* Random events
* Policing / Military
* Planet development
* UI
* Map
* Random seed
* Seed input and output
* Font
* Explore
* Expand / Colonize
* Planet overview
* Red and blue energy
