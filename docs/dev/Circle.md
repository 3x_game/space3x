# Circle

This document contains the main idea how to draw circles and some references.

## Concept

* Use midpoint algorithm
  * First step: just the circle
  * Second step: filled circle

## References

* [Kreis und Kreisgleichung](https://mathepedia.de/Kreis.html)
* [Square root of an integer](https://www.geeksforgeeks.org/square-root-of-an-integer/)
* [The Midpoint Circle Algorithm Explained Step by Step](https://www.youtube.com/watch?app=desktop&v=hpiILbMkF9w)
* [The Midpoint Circle Algorithm](https://en.m.wikipedia.org/wiki/Midpoint_circle_algorithm)
* [Rasterizing a circle](http://www.mindcontrol.org/~hplus/graphics/RasterizeCircle.html)
* [Alois Zingl - A Rasterizing Algorithm for Drawing Curves](A Rasterizing Algorithm for Drawing Curves)
* [A Rasterizing Algorithm for Drawing Curves](http://members.chello.at/~easyfilter/bresenham.html)
