# Done state

This document describes the "Done" state or all features that I consider minimum for this project to be "finished".
This is inspired by [The Art of Finishing](https://www.bytedrum.com/posts/art-of-finishing/).

## Development

* Define debug/development build (with logging etc.) and a production build (without logging).
  * [Set compile time constant](https://ziggit.dev/t/set-compile-time-constant/6105/2)
* Set-up debugging for tests in Neovim
  * [Debugging Zig: with lldb and inside Neovim](https://eliasdorneles.com/til/posts/customizing-neovim-debugging-highlight-zig-debug-w-codelldb/)
  * [Neovim setup for zig](https://terminalprogrammer.com/neovim-setup-for-zig)
  * [Debugging Zig Applications](https://pedropark99.github.io/zig-book/Chapters/02-debugging.html)
  * [Ziggit forum thread Debugging Zig (with a debugger)](https://ziggit.dev/t/debugging-zig-with-a-debugger/7160)

## Graphics

Only the relevant graphical elements are realized:

* Rectangles
* Circles
* Simple Numbers, Capital Letters and some punctuation
* Simple Icons for Buttons like
  * Research
  * Next Turn
  * Undo changes
  * Energy "crystals" / "blitz"
* Optional: Maybe planets are not just "circles" but have "structure"
* Optional: Each planet is slightly distorted from with middle point, smaller planets more, larger planets less
* Optional: Add some number of white points in the background to "simulate" some further stars

## Sound

* No sound effects
* Optional: space / spheric music

## Gameplay

* Explore new areas / planets
* Colonize new planets
* Research improvements
  * Colony ship types for landing on different planets
  * Cheaper exploration
  * Cheaper colony ships
  * More energy storage
  * Better energy production
  * Better military / police ships
* Random bad / good events
  * Random events have usually an option to resolve them, example given, through research
* Blue energy, not saved between turns
* Red energy, saved between turns
* Develop planets, i.e., improve them
  * Usually results in more energy production and larger population limit
* Energy production / energy consumption level per planet
  * The more advanced a planet is the more this can get out of balance so that a planet is a net energy importer
  * Planets may shrink when energy influx if too few energy
* Option: Policing / happiness / uprising / rogue planet system
  * Build "military/police" ships
  * I.e., a simple fleet management
* Random seeds can be used / re-used / displayed

## Manual & Help

* Manual is created in html to avoid complex text setting in game
