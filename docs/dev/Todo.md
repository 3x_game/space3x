# Todos

* Font
  * Create an individual instance of this struct for each character or icon as
    static public method
* Test page
  * Create a complete test page with all characters and graphical elements to
    check if they are rendered correctly
* Introduce ZII (Zero Is Initialization) for all data types
* Optimize for smart phone / fairphone 4 / full-hd
* Implement a game graphics module on top of graphics
  * Example: `draw_start_menu`
* Implement game logic following principles of BDD
  * Example, creating specifications (using unit tests) for how the user will use the game
  * E.g., `fn start_new_game(seed:u32)`, `query_quadrant`, `colonize_planet` etc.
