# Ideas

This document contains ideas I get during development which I may implement later when the project is [Done](./Done.md).
The concept is to finish the game and add / improve later.

## SW Design

* Extract Drawable interface?
* Extract frame buffer interface?
* Or redesign using a Renderer / Rasterizer interface?
  * The renderer actually only takes simple geometric primitive
  * In the SW rasterization implementation it then actually creates the rectangles etc. on the fly and uses a frame buffer
  * But a different interface for the Renderer could be used and this could use a GPU
* Diff-based rendering
  * Since only grid cells are usually re-drawn it makes sense to implement a diff-based rendering, which only re-draws the affected cells.

## Color space

* Use less than 32 bits for the rgba values
* Maybe use u8 with 2 bits per color
  * and 2 bits for alpha, which enables anti-aliasing with alpha
* Or use u4 with 1 bit per color. This at least yields 8 colors
* [Fast Unorm Conversions](https://rundevelopment.github.io/blog/fast-unorm-conversions)

## Computer enemy

* Use deep learning to create AI on raw game state as output and (x,y) as input
* [Building a Neural Network from Scratch in Rust](https://dev.to/evolvedev/building-a-neural-network-from-scratch-in-rust-10aa)
* [Summary of Ilya Sutskevers AI Reading List](https://tensorlabbet.com/2024/09/24/ai-reading-list/)

## Manual / Help

* The manual an help is rendered in game graphics
* Text is automatically wrapped and pages added with up and down buttons based on the grid

## Font

* Font can be a different size then optimized to 100x100 pixels

## Dynamic canvas

* The canvas can be dynamic and the game pretty much renders on this size

## Advanced Graphics

* Anti-Aliasing
  * [AAA - Analytical Anti-Aliasing](https://blog.frost.kiwi/analytical-anti-aliasing/)
