# space3x

A Zig & WebAssembly version of my video game that is inspired by Master of Orion (but by no means a re-implementation).

## License

This program is licensed under AGPLv3, see [COPYING](./COPYING).
It was licensed under MIT before and then GPLv3, but is no longer.
I believe that there is value in the game I am writing an I have no problem if someone picks these ideas up and uses them as permitted under AGPLv3, commercial or not, but I want this to stay open source as defined by AGPLv3.

## Font

The font in this game is designed by myself using only rectangles and circles and is therefore not in any normal font file format.
The fact that it is limited two these geometric forms, it obviously resembles other fonts that use mostly horizontal and vertical lines.
There are other fonts that are also rather simple in particular [Square Sans Serif](https://www.1001fonts.com/square-sans-serif-7-font.html).
This font also uses some diagonal lines, that is why I cannot use it directly.
However, Square Sans Serif does also use a lot of horizontal and vertical lines that's why some letters or numbers between my font and Square Sans Serif look similar.
That's why I providing a backlink to their font here as well as I have copied the [readme.txt](./docs/dev/font/Square_Sans_Serif/readme.txt) of this font into this repository.
This readme.txt provides the necessary information on the fact that Square Sans Serif font is freeware and can be used in commercial products.
The font I created for this game is licensed under AGPLv3 and copyright is on me.
